import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from './views/HelloWorld.vue'
import Nodes from './views/Nodes.vue'
import Relations from './views/Relations.vue'
import Database from './views/DataBase.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/nodes',
      name: 'nodes',
      component: Nodes
    },
    {
      path: '/relations',
      name: 'relations',
      component: Relations
    },
    {
      path: '/database',
      name: 'dataBase',
      component: Database
    }
  ]
})