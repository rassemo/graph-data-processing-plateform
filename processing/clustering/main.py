#from clustering import functs
from typing import Optional
from neo4j import GraphDatabase
import pandas as pd
from functs import clustering_kmeans, kmeans_text, hca_text, trans_data
import argparse


driver=GraphDatabase.driver(uri="neo4j://neo4j:7687",auth=("neo4j","password"))
session=driver.session()

def get_nodes(label):
    req = f'MATCH (n:{label}) RETURN n'
    results = session.run(req)
    data = results.data()
    bk = []
    for el in data:
        cont = el['n'] 
        bk.append(cont)
    df = pd.DataFrame(bk)
    return df

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-c1', action="store") # name of the first column for clustering
    parser.add_argument('-c2', action="store") # name of the second column for clustering
    parser.add_argument('-nc', action="store") # number of clusters
    parser.add_argument('-cj', action="store") # nom de la colonne de jointure 
    parser.add_argument('-nd', action="store") # 'Book,Review'
    parser.add_argument('-ct', action="store") # clustering avec du texte (nom de la colonne, nbr de centroides, nbr de clusters) 'summary',300,2
    args = parser.parse_args()

    l = args.nd.split(',')
    
    if len(l) > 1:
        df1 = get_nodes(l[0])
        df2 = get_nodes(l[1])
        data = pd.merge(df1, df2, on=args.cj)
        
    else:
        data = get_nodes(args.nd)
    
    if args.ct is None:
        res =clustering_kmeans(data, args.c1, args.c2, int(args.nc))
        print(res)  

    else : 
        t = args.ct.split(',')
# ~~~~~~~~~~ Clustering en utilisant une représentation vectorielle du texte
# Prétraitement des données et représentation vectorielle
        df = trans_data(data, t[0],(1,1),5, 0.7)
# clustreing avec kmeans
        centers = kmeans_text(df,int(t[1]))
# clustering avec HCA
        labels = hca_text(centers, int(t[2]))
        print(labels)
