import pandas as pd
import numpy as np
from functs import clustering_kmeans, kmeans_text, hca_text, text_processing, constr, build_vocab
from main import get_nodes


def test_get_nodes():
    """ get_nodes() -> vérifier le type de retour, df non vide """
    df = get_nodes('Book')
    df2 = get_nodes('Review')
    assert (df.empty == False) and (df2.empty == False), "Cannot get data from database"


def test_clustering_size():
    """ clustering_kmeans() -> comparer la taille de dataset en entrée / sortie,  """
    X = np.array([[5,3],
     [10,15],
     [15,12],
     [24,10],
     [30,45],
     [85,70],
     [71,80],
     [60,78],
     [55,52],
     [80,91],])
    data = pd.DataFrame(X, columns=['col1','col2'])
    res = clustering_kmeans(data, 'col1', 'col2', 2)
    assert len(res) == 10, "Should be equal to 10"


def test_clustering_interval():
    """ vérifier les valeurs de la colonne des labels qui doit contenir autant de label que de nbr de classe """
    X = np.array([[5,3],
     [10,15],
     [15,12],
     [24,10],
     [30,45],
     [85,70],
     [71,80],
     [60,78],
     [55,52],
     [80,91],])
    data = pd.DataFrame(X, columns=['col1','col2'])
    res = clustering_kmeans(data, 'col1', 'col2', 2)
    assert max(res['label']) < 2, "Should be smaller than nb_clust"



def test_text_processing():
    """ text_processing : vérifier entrée / sortie """
    assert text_processing('The man @Tom 100 went out for a walk !') == 'man tom went walk', "Should be corrected"




def test_build_vocab():
    """ tester la construction du vocab """
    corpus =  ['born potential',
    'born goodness trust',
    'born ideal dream',
    'born greatness',
    'born wing',
    'meant crawling',
    'wing',
    'learn use fly']
    data = pd.DataFrame(corpus, columns=['data'])
    assert build_vocab(data,'data',(1,1), 1, 8) == ['born', 'crawling', 'dream', 'fly','goodness', 'greatness','ideal','learn','meant','potential','trust','use','wing'], "Error when constructing the vocab"



def test_rep_vectorielle():
    """ tester la représentation vectorielle"""
    corpus =  ['born potential',
    'born goodness trust',
    'born ideal dream',
    'born greatness',
    'born wing',
    'meant crawling',
    'wing',
    'learn use fly']
    data = pd.DataFrame(corpus, columns=['data'])
    vocab = ['born', 'crawling', 'dream', 'fly','goodness', 'greatness','ideal','learn','meant','potential','trust','use','wing']
    representation = np.array([[0.49, 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.87, 0.  ,
        0.  , 0.  ],
       [0.37, 0.  , 0.  , 0.  , 0.66, 0.  , 0.  , 0.  , 0.  , 0.  , 0.66,
        0.  , 0.  ],
       [0.37, 0.  , 0.66, 0.  , 0.  , 0.  , 0.66, 0.  , 0.  , 0.  , 0.  ,
        0.  , 0.  ],
       [0.49, 0.  , 0.  , 0.  , 0.  , 0.87, 0.  , 0.  , 0.  , 0.  , 0.  ,
        0.  , 0.  ],
       [0.56, 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  ,
        0.  , 0.83],
       [0.  , 0.71, 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.71, 0.  , 0.  ,
        0.  , 0.  ],
       [0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  ,
        0.  , 1.  ],
       [0.  , 0.  , 0.  , 0.58, 0.  , 0.  , 0.  , 0.58, 0.  , 0.  , 0.  ,
        0.58, 0.  ]])
    res = np.array(round(constr(data, 'data', vocab,(1,1), 1, 8),2))
    assert np.array_equal(representation,res),  "Error in the vectorization"
        


def test_kmeans_text():
    """ faire le kmeans sur du texte  """
    X = np.array([[0.49, 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.87, 0.  ,
        0.  , 0.  ],
       [0.37, 0.  , 0.  , 0.  , 0.66, 0.  , 0.  , 0.  , 0.  , 0.  , 0.66,
        0.  , 0.  ],
       [0.37, 0.  , 0.66, 0.  , 0.  , 0.  , 0.66, 0.  , 0.  , 0.  , 0.  ,
        0.  , 0.  ],
       [0.49, 0.  , 0.  , 0.  , 0.  , 0.87, 0.  , 0.  , 0.  , 0.  , 0.  ,
        0.  , 0.  ],
       [0.56, 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  ,
        0.  , 0.83],
       [0.  , 0.71, 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.71, 0.  , 0.  ,
        0.  , 0.  ],
       [0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  , 0.  ,
        0.  , 1.  ],
       [0.  , 0.  , 0.  , 0.58, 0.  , 0.  , 0.  , 0.58, 0.  , 0.  , 0.  ,
        0.58, 0.  ]])
    vocab = ['born', 'crawling', 'dream', 'fly','goodness', 'greatness','ideal','learn','meant','potential','trust','use','wing']
    data = pd.DataFrame(X, columns= [vocab])
    centers = kmeans_text(data,4)
    assert len(centers) == 4, "Should be equal to 4"


def test_hca():
    """ tester hca_text """
    X = np.array([[5,3],
     [10,15],
     [15,12],
     [24,10],
     [30,45],
     [85,70],
     [71,80],
     [60,78],
     [55,52],
     [80,91],])
    data = pd.DataFrame(X, columns=['col1','col2'])
    res= hca_text(data, 3)
    label = pd.DataFrame(res, columns=['label'])
    assert (max(label['label']) < 3) and (min(label['label']) >= 0), "Should be in the interval "



if __name__ == "__main__":
    test_get_nodes()
    test_clustering_size()
    test_clustering_interval()
    test_text_processing()
    test_build_vocab()
    test_rep_vectorielle()
    test_kmeans_text()
    test_hca()
    print("Everything passed")