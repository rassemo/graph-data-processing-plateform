import matplotlib.pyplot as plt
#%matplotlib inline
import numpy as np
from sklearn.cluster import KMeans
from sklearn.preprocessing import normalize
import pandas as pd
import re
import scipy.cluster.hierarchy as shc
from sklearn.cluster import AgglomerativeClustering
from sklearn.feature_extraction.text import TfidfVectorizer
import regex
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
import string
from nltk.stem import WordNetLemmatizer 
nltk.download('wordnet')
# from fastapi import FastAPI, Request, HTTPException
# from fastapi.middleware.cors import CORSMiddleware
# import uvicorn 

# app = FastAPI()
# # api middleware
# app.add_middleware(
#     CORSMiddleware,
#     allow_origins=['http://localhost:8000','http://localhost:8080','http://localhost:4040','http://localhost:90','http://localhost:70','http://127.0.0.1:8000','http://127.0.0.1:8080'],
#     allow_credentials=True,
#     allow_methods=['*'],
#     allow_headers=['*'],
# )
# app.get("/getData")
# def fun():
#     return "hello World"

def clustering_kmeans(data, col1, col2, nb_clust):
    df = data[[col1,col2]]
    data_scaled = normalize(df)
    data_scaled = pd.DataFrame(data_scaled, columns=df.columns)
    col = df.columns
    X = data_scaled.to_numpy()
    kmeans = KMeans(n_clusters= nb_clust)
    kmeans.fit(X)
    labels = list(kmeans.labels_)
    classe = pd.DataFrame(labels, columns=['label'])
    res = pd.concat([data, classe], axis=1)
    # partie visualisation 
    plot = plt.scatter(X[:,0],X[:,1], c=kmeans.labels_, cmap='PiYG_r') # génération du plot
    plot.figure.savefig('./plot.png') # enregistrement du plot obtenu comme une image au format png
    return res[[col1,col2, 'label']]


####################################### NLP et clustering ############################################

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  Fonctions de prétraitement du texte ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def delete_url(text) : 
    """ supression url  """
    return re.sub(r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', text)
  

def delete_balise(text) : 
    """ supression balise html  """
    text = re.sub('<[^<]+?>', '', text)
    text = text.replace('<a href="', ' ')
    return text


def convert_spec(text) :
    """ supression des caractères spéciaux """
    text = re.sub('&#aq;', "'", text) 
    text = re.sub('&quot;', "'", text)
    text = re.sub('&#39;', "'", text)
    return text 


def lowercase(text) : 
    """ lowecase text """
    return text.lower()  

        
def replace_symbols(text) :
    """ replace REPLACE_BY_SPACE_RE symbols by space in text """
    REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|,;]')
# replace REPLACE_BY_SPACE_RE symbols by space in text
    text = REPLACE_BY_SPACE_RE.sub(' ', text)
    return text


def delete_ponct(text) : 
    """ delete ponctuation """
    return re.sub(u"[^\w\d'\s]",'',text)


def delete_stop(text) :  
    """ delete stopwors from text """
    STOPWORDSEN = set(stopwords.words('english'))
    # delete english stopwords from text
    text = ' '.join(word for word in text.split() if word not in STOPWORDSEN)
    return text    


def delete_spaces(text) :
    """ suppression des espaces en plus """
    text = ' '.join(text.split())
    return text
    #return res


def delete_numbers(text) :
    """ suppression des nombres """
    res = re.sub(r'[0-9]+', '', text)
    return res


def lem(text):
    """ lemmatisation du texte """
    lemmatizer = WordNetLemmatizer() 
    words = text.split()
    res = []
    for word in words:
        res.append(lemmatizer.lemmatize(word))
    return ' '.join(res)


def text_processing(text):
    """ fonction globale des prétraitements """
    # supression url 
    text = delete_url(text)
    # supression balise html 
    text = delete_balise(text)
    # conversion des caractères spéciaux
    text = convert_spec(text)
    # lowecase text
    text = lowercase(text)  
    # replace REPLACE_BY_SPACE_RE symbols by space in text
    text = replace_symbols(text)
    # delete stopwors from text
    text = delete_stop(text)
    # delete spaces
    text = delete_spaces(text)
    # delete ponctuation
    text = delete_ponct(text)
    # delete numbers
    text = delete_numbers(text)
    #lemmatisation
    text = lem(text)
    return text




#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Fonctions de représentation vectorielle du texte ~~~~~~~~~~~~~~~~~~~~



def listToString(s):  
    """ transformation d'une liste en chaine de caractère """
    str1 = "" 
    return (str1.join(str(s))) 


def build_vocab(df,col,ngrange, mindf, maxdf):
    """ construction du vocabulaire """ 
    desc = []
    for index,row in df.iterrows():
        desc.append(listToString(row[col]))
    vectorizer = TfidfVectorizer(sublinear_tf=True,ngram_range=ngrange, min_df=mindf, max_df=maxdf, norm='l2') 
    vectors = vectorizer.fit(desc) 
    feature_names = vectorizer.get_feature_names()
    vocab = feature_names
    return vocab


def constr(df, col, vocab, ngrange, mindf, maxdf):
    """ vectorisation """
    corpus = []
    for index,row in df.iterrows():
        corpus.append(listToString(row[col]))   
    vectorizer = TfidfVectorizer(sublinear_tf=True, vocabulary= vocab, min_df=mindf, max_df=maxdf, norm='l2') 
    res = vectorizer.fit_transform(corpus) 
    dense = res.todense()
    denselist = dense.tolist()
    ngram = pd.DataFrame(denselist, columns=vocab)
    return ngram

def rep_vect(txt,col, ngram, mindf, maxdf):
    """ représentation vectorielle """
    vocab = build_vocab(txt,col,ngram, mindf, maxdf)
    X = constr(txt, col,vocab,ngram, mindf, maxdf)
    return X



def trans_data(data, col,ngram, mindf, maxdf):
    """ Transformation des données de notre dataset  
    data désigne le nom du dataframe et col le nom de la colonne de type texte que l'on veut utiliser dans le clustering 
    """
    text = data[[col]]
    new = col+'_trans'
    text[new]= text[col].apply(text_processing)
    df = rep_vect(text, new,ngram, mindf, maxdf)    
    return df


def kmeans_text(df,nb_clust):
    """ algorithme kmeans sur du texte"""
    X = df.to_numpy()
    kmeans = KMeans(n_clusters=nb_clust)
    kmeans.fit(X)
    res = kmeans.cluster_centers_
    #print(kmeans.cluster_centers_)
    #print(kmeans.labels_)
    return res


def hca_text(df, nb_clust):
    """ algorithme HCA sur les centroidess """
    cluster = AgglomerativeClustering(n_clusters=nb_clust, affinity='euclidean', linkage='ward')  
    res = cluster.fit_predict(df)
    return res