from neo4j import GraphDatabase
import json

class Data_Base:

    def __init__(self, uri, login, password, meta_path):
        
        driver = GraphDatabase.driver(uri = uri, auth=(login,password))
        self.session = driver.session()

        with open(meta_path) as f:
            self.meta = json.load(f)


    def insert_nodes(self,node_list):

        cypher = """
            UNWIND $nodes as row 
            MERGE (n: {labels} {{{props}}}) 
            SET n += row
        """

        for key, nodes in node_list:

            sf = self.sub_format(key)
            req = cypher.format(labels=sf['labels'], props=sf['unique'])
            self.session.run(req,{'nodes':nodes})

            print(f'merge { len(nodes) } {self.meta[key]["labels"]} nodes\n-------------------->')

    def insert_relations(self, relation_list):

        cypher = """
            UNWIND $relations as row 
            MERGE (from:{src_labels} {{{src_props}}})               
            MERGE (to:{targ_labels} {{{targ_props}}})
            MERGE (from)-[relation:{label}]->(to)
            SET relation += row.rel
        """

        for key, relations in relation_list:

            src_sf = self.sub_format(key[1],'row.src')
            targ_sf = self.sub_format(key[2],'row.targ')

            req = cypher.format(src_labels=src_sf['labels'], src_props=src_sf['unique'], targ_labels=targ_sf['labels'], targ_props=targ_sf['unique'], label=key[0])
            self.session.run(req,{'relations':relations})

            print(f'merge { len(relations) } {key[0]} relations\n-------------------->')

    # format subsequences of cypher queries
    def sub_format(self,key,row_name='row'):

        meta = self.meta[key]
        labels, unique = meta['labels'] ,meta['unique']

        return {
            # input : ['prop1', 'prop2', ...] => output : 'prop1:prop2: ...'
            'labels': ':'.join(labels),
            # input : ['prop1', 'prop2', ...], 'row' => output : 'row.prop1, row.prop2, ...'
            'unique': ', '.join(map(lambda x: f'{x}: {row_name}.{x}',unique))
        }
    
    # close connection
    def close(self):
        self.session.close()
