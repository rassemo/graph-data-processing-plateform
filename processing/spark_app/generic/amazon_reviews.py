import json

def extract_nodes(row):
    """
    For each entry, return a list of dicts: [{}, ...]
    dict = {
        'key': "Node type identifier, specified in meta.json file",
        'props': { node properties }
    }
    """

    row = json.loads(row[1])

    def calcul(liste):
        """ 
        calculer le dégré d'aide du commentaire de la review 
        """
        if int(liste[1]) == 0 : 
                helpf = 0
        else:
            helpf = int(liste[0])/int(liste[1])
        return helpf

    return [
        {
            'key':'n2',
            'props':{
                'reviewerID':str(row.get("reviewerID","")),
                'asin':str(row.get("asin","")),
                'reviewerName':row.get("reviewerName",""),
                'helpful':float(calcul(list(''.join( c for c in str(row.get("helpful","")) if  c not in '?:!/;,][' ).split()))),
                'reviewText':row.get("reviewText",""),
                'overall':float(row.get("overall","")),
                'summary':row.get("summary",""),
                'unixReviewTime':row.get("unixReviewTime",""),
                'reviewTime':row.get("reviewTime",""),
                'reviewSize':len(row.get("reviewText").split())
            }
        }
    ]


def extract_relations(row):
    """
    For each entry, return a list of dicts: [{}, ...]
    dict = {
        'key': ('label', src_node_id, targ_node_id),
        'props': {
            'src':{ source node properties },
            'targ':{ target node properties },
            'rel':{ relationship properties },
        }
    }
    """

    return []

