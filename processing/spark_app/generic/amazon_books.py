import json
import re

def extract_nodes(row):
    """
    For each entry, return a list of dicts: [{}, ...]
    dict = {
        'key': "Node type identifier, specified in meta.json file",
        'props': { node properties }
    }
    """

    row = json.loads(row[1])

    return [
        {
            'key':'n1',
            'props':{
                'asin':str(row.get("asin","")),
                'categories':str(row.get("categories","")),
                'description':row.get("description",""),
                'title':row.get("title",""),
                'price':float(row.get("price","-1")),
                'salesRank':int(str(re.findall('\d+', str(row.get("salesRank",-1)))[0])),
                'imUrl':row.get("imUrl","")
            }
        }
    ]


def extract_relations(row):
    """
    For each record in the dataset, return a list of dicts: [{}, ...]
    dict = {
        'key': ('label', src_node_id, targ_node_id),
        'props': {
            'src':{ source node properties },
            'targ':{ target node properties },
            'rel':{ relationship properties },
        }
    }
    """

    row = json.loads(row[1])
    
    relations = []

    if 'related' in row:

        for label,ids_list in row['related'].items():

            for item in ids_list:
                relations.append(
                    {
                        'key':(label,'n1','n1'),
                        'props':{
                            'src':{'asin':row['asin']},
                            'targ':{'asin':item},
                            'rel':{}
                        }
                    }
                )
                
    return relations

