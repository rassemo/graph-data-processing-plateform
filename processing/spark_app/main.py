from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
from database import Data_Base
import json
from pathlib import Path
import argparse

# Setup application
def init():

    # argeument parser setup
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', action="store") # file name containing user defined funcs
    parser.add_argument('-m', action="store") # graph-metadata file name
    args = parser.parse_args()

    # init spark context
    sc = SparkContext(appName="graph-data-processor")
    sc.setLogLevel('WARN')

    # current working direcory
    curdir = str(Path(__file__).parent.absolute())
    
    # load configuration file
    with open(curdir+'/config.json') as f:
        config = json.load(f)

    # init spark streaming
    ssc = StreamingContext(sc, config['interval'])

    # import user defined module
    sc.addPyFile(curdir + '/generic/' + args.f)
    md = __import__(Path(args.f).stem)

    # connect to neo4j
    db = Data_Base(
        config['neo4j_uri'], config['neo4j_login'], config['neo4j_password'],
        curdir +'/generic/'+ args.m
    )

    # connect to kafka
    ds = KafkaUtils.createDirectStream(ssc, 
        [config['topic']],
        {"metadata.broker.list":config['broker']}
    )

    return ssc, db, ds, md

# apply transformations
def transform(dstream,extractor):
    return dstream.flatMap(extractor) \
            .map(lambda x: (x['key'], [x['props']])) \
            .reduceByKey(lambda x,y: x+y)

# save to database
def persist(dstream,insert_func):
    dstream.foreachRDD(lambda rdd:insert_func(rdd.collect()))

if __name__ == "__main__":

    # Init application components
    ssc, db, ds, md = init()

    # Data Transformations
    nodes = transform(ds, md.extract_nodes)
    relations = transform(ds, md.extract_relations)

    # Insert into database
    persist(nodes, db.insert_nodes)
    persist(relations, db.insert_relations)

    # Start opp
    ssc.start()
    
    try:
        ssc.awaitTermination()
    finally:
        print('\n--------------\nEnd streaming\n--------------\n')
        db.close()

    