import findspark  # this needs to be the first import
findspark.init()
import pytest
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
import json
import sys

sys.path.append("../")
from dstream import simulate_dstream, collect_dstream
from main import transform
from data import input_data, output_data
import funcs as md

@pytest.fixture
def spark_context():
    sc = SparkContext(appName="test-app")
    sc.setLogLevel('WARN')
    return sc

@pytest.fixture
def streaming_context(spark_context):
    return StreamingContext(spark_context,1)

def test_nodes(spark_context, streaming_context):

    ds = simulate_dstream(spark_context, streaming_context, input_data)
    
    nodes = transform(ds,md.extract_nodes)
    res = collect_dstream(streaming_context, nodes, 2)

    assert res == output_data['nodes']
    
def test_relations(spark_context, streaming_context):

    ds = simulate_dstream(spark_context, streaming_context, input_data)
    
    relations = transform(ds,md.extract_relations)
    res = collect_dstream(streaming_context, relations, 2)

    assert res == output_data['relations']





















