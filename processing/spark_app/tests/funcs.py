
def extract_nodes(row):

    return [
        {
            'key':'n1',
            'props':{
                'event_id':row['event_id'],
                'device_ref':row['device_ref'],
                'signal':row['signal'],
                'level':row['level']
            }
        },
        {
            'key':'n2',
            'props':{
                'category_id':row['category']['category_id'],
                'name':row['category']['name']
            }
        }
    ]

def extract_relations(row):
    
    return [
        {
            'key': ('BELONGS_TO', 'n1', 'n2'),
            'props': {
                'src':{ 'event_id':row['event_id'] },
                'targ':{ 'category_id':row['category']['category_id'] },
                'rel':{},
            }
        }
    ]