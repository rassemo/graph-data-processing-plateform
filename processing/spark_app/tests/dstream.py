import time

# make a dstream from python list
def simulate_dstream(sc, ssc, data):
    
    h = len(data)//2
    
    RDD1 = sc.parallelize(data[:h],1)
    RDD2 = sc.parallelize(data[h:],1)

    return ssc.queueStream([RDD1,RDD2])

# collect dstream items as a python list
def collect_dstream(ssc, dstream, expected_length):

    result = []

    def get_output(rdd):
        if rdd and len(result) < expected_length:
            r = rdd.collect()
            if r:
                result.append(r)
    
    dstream.foreachRDD(get_output)

    ssc.start()

    timeout = 5
    start_time = time.time()

    while len(result) < expected_length and time.time() - start_time < timeout:
        time.sleep(0.01)

    if len(result) < expected_length:
        print("timeout after", timeout)

    return result