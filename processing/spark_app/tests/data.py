input_data = [
    {
        "event_id":"e1",
        "device_ref":"dev0001",
        "signal":"xxx",
        "level":2,
        "category":{
            "category_id":"c1",
            "name":"gama"
        }
    },
    {
        "event_id":"e2",
        "device_ref":"dev0001",
        "signal":"xxx",
        "level":4,
        "category":{
            "category_id":"c1",
            "name":"gama"
        }
    },
    {
        "event_id":"e3",
        "device_ref":"dev0001",
        "signal":"zzz",
        "level":9,
        "category":{
            "category_id":"c2",
            "name":"omega"
        }
    },
    {
        "event_id":"e4",
        "device_ref":"dev0001",
        "signal":"yyy",
        "level":7,
        "category":{
            "category_id":"c2",
            "name":"omega"
        }
    },
    {
        "event_id":"e5",
        "device_ref":"dev0001",
        "signal":"zzz",
        "level":8,
        "category":{
            "category_id":"c2",
            "name":"omega"
        }
    }
]

output_data = {
    "nodes":[
        [# from RDD1
            ("n1", [{"event_id": "e1", "device_ref": "dev0001", "signal": "xxx", "level": 2}, {"event_id": "e2", "device_ref": "dev0001", "signal": "xxx", "level": 4}]),
            ("n2", [{"category_id": "c1", "name": "gama"}, {"category_id": "c1", "name": "gama"}])
        ], 
        [# from RDD2 
            ("n1", [{"event_id": "e3", "device_ref": "dev0001", "signal": "zzz", "level": 9}, {"event_id": "e4", "device_ref": "dev0001", "signal": "yyy", "level": 7}, {"event_id": "e5", "device_ref": "dev0001", "signal": "zzz", "level": 8}]), 
            ("n2", [{"category_id": "c2", "name": "omega"}, {"category_id": "c2", "name": "omega"}, {"category_id": "c2", "name": "omega"}])
        ]       
    ],
    
    "relations":[
        [# from RDD1
            ( ('BELONGS_TO', 'n1', 'n2') , [{'src': {'event_id': 'e1'}, 'targ': {'category_id': 'c1'}, 'rel': {}}, {'src': {'event_id': 'e2'}, 'targ': {'category_id': 'c1'}, 'rel': {}}])
        ], 
        [# from RDD2
            ( ('BELONGS_TO', 'n1', 'n2') , [{'src': {'event_id': 'e3'}, 'targ': {'category_id': 'c2'}, 'rel': {}}, {'src': {'event_id': 'e4'}, 'targ': {'category_id': 'c2'}, 'rel': {}}, {'src': {'event_id': 'e5'}, 'targ': {'category_id': 'c2'}, 'rel': {}}])
        ]
    ]
}