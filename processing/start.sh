#!/bin/bash

$SPARK_HOME/bin/spark-submit \
--master local[*] \
--packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.0.0 \
--conf spark.jars.ivy=/tmp/.ivy \
spark_app/main.py -f amazon_books.py -m amazon_meta.json
