# Plateforme pour l’ingestion, transformation et export de données de graphs

## Ingestion
Simulation d'un flux de données, par un script qui lit un dataset statique, et écrit les données (brutes) dans Kafka. Un vrai flux de données continu peut être utilisé.

## Processing
Application Spark qui lit les données qui arrivent au fure et à mesure dans kafka, les transforme et les stocke dans la base de données Graph.

## Export
API REST qui permet d'exporter à la demande, des sous-graphs (différents formats)

## Frontend
Visualisation des données

# Prérequis
- Connecter le source de données à Kafa (via un script, ou un autre moyen)
- Fournir le fichier (.py) contenant les 2 fonctions d'extraction des noeuds et relations + le fichier de configuration (.json) contenant les métadatas.
- Placer les 2 fichiers dans le dossier /spark_app/generic (voir le cahier de conception)

# Démarrage

###### Lancer les contenaires :
    docker-compose up

###### Générer le flux de données :
    python ./generate_stream.py chemin/vers/dataset
    
###### Lancer l'application Spark Streaming :
    docker exec -it spark bash ./start.sh

###### Lancer le clustering :
    docker exec -it spark bash -c 'python3 ./clustering/main.py'

###### Ports (voir le fichier docker-compose.yml)
    Neo4j : 7474, FastAPI : 90, Vuejs : 70
