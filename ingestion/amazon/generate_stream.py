from kafka import KafkaProducer
import json
import gzip
import sys
from time import sleep

# https://jmcauley.ucsd.edu/data/amazon/

def parse(path):
  for item in gzip.open(path, 'r'):
  	yield eval(item)

if __name__ == "__main__":
	
	if (len(sys.argv) != 2):
		print("Usage: python3 generate_stream.py <file-path>")
		sys.exit(-1)

	data = parse(sys.argv[1])
	
	producer = KafkaProducer(
		bootstrap_servers = ['localhost:29092'],
		value_serializer = lambda m: json.dumps(m).encode('utf-8')
	)

	for item in data:
		print(item)
		producer.send('graph-data',item)
		sleep(0.2)