from fastapi import FastAPI, Request, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from neo4j import GraphDatabase
import networkx as nx
from networkx.readwrite import json_graph
from typing import Optional
import app.cypher_formatters as cf

app = FastAPI()

# api middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=['http://localhost:8000','http://localhost:8080','http://localhost:70','http://127.0.0.1:8000','http://127.0.0.1:8080'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

# Database Connexion
driver=GraphDatabase.driver(uri="neo4j://neo4j:7687",auth=("neo4j","password"))
session=driver.session()

# run cypher queries
def run_query(cypher):
    try:
        return session.run(cypher)
    except:
        raise HTTPException(status_code=500, detail="Unvalid Cypher Syntax, check your params ...")

# format graph data
def format_data(graph,format):
    switch = {
        'json': json_graph.node_link_data,
        'gml': nx.generate_gml,
        'gexf':nx.generate_gexf,
        'graphml':nx.generate_graphml,
        'pajek':nx.generate_pajek
    }

    formatter = switch.get(format, json_graph.node_link_data)
    return formatter(graph)

# root
@app.get("/")
def serve_home(request: Request):
    return {'host':request.client.host, 'port':request.client.port}

# export graphs
@app.get("/graph")
def get_graph(
    rel_types: Optional[str] = "",
    rel_filter: Optional[str] = "",
    src_labels: Optional[str] = "",
    src_filter: Optional[str] = "",
    trg_labels: Optional[str] = "",
    trg_filter: Optional[str] = "",
    format: Optional[str] = "json",
    limit:Optional[str] = ""
):  

    rel = cf.format_relation_types(rel_types,'rel')
    src = cf.format_labels(src_labels,'src')
    trg = cf.format_labels(trg_labels,'trg')
    
    filter = cf.format_filter(rel=rel_filter, src=src_filter, trg=trg_filter)

    cypher = f'MATCH ({src})-[{rel}]->({trg}) WHERE {filter} RETURN src, trg, rel {limit}'
    print(cypher)

    result = run_query(cypher)

    graph = nx.Graph()

    for record in result:

        graph.add_node(record['src'].id,labels=','.join(record['src'].labels), **dict(record['src'].items()))
        graph.add_node(record['trg'].id,labels=','.join(record['trg'].labels), **dict(record['trg'].items()))
        
        graph.add_edge(
            record['rel'].start_node.id,
            record['rel'].end_node.id,
            id=record['rel'].id,
            type=record['rel'].type,
            **dict(record['rel'].items())
        )
    
    return format_data(graph,format)

# export nodes only
@app.get("/graph/nodes")
def get_nodes(labels: Optional[str] = "", filter: Optional[str] = "", format: Optional[str] = "json", limit:Optional[str] = ""):

    labels = cf.format_labels(labels,'n')
    filter = cf.format_filter(n=filter)

    cypher=f'MATCH ({labels}) WHERE {filter} RETURN n {limit}'
    print(cypher)

    result = run_query(cypher)

    graph = nx.Graph()

    for record in result:  
        graph.add_node(record['n'].id,labels=','.join(record['n'].labels), **dict(record['n'].items()))      
    
    return format_data(graph,format)