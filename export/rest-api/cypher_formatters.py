
#------------------------------#
# format cypher query segments #
#------------------------------#

def format_labels(input_labels, alias):
    if (input_labels != ""): return alias + ':' + input_labels.replace(',',':')
    else: return alias

def format_relation_types(input_types, alias):
    if(input_types!=""): return alias + ':'+ input_types.replace(',','|') 
    else: return alias

def format_filter(**kwargs):

    op = " AND "
    filter = ""

    for alias, input_str in kwargs.items():
        if(input_str != ""): 
            filter += f'{alias}.'+ input_str.replace(',',f' AND {alias}.') + op

    if(filter!=""): filter = filter[:-len(op)]
    else: filter = "True" # No filter
    
    return filter
